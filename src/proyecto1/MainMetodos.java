
package proyecto1;

/**
 *
 * @author Carlos
 */
public class MainMetodos {
    public static void main(String[] args) 
    {
        int nodos = 100;
        int aristas = 500;
        double proba = 0.6;
        float distancia = 20;
        Proyecto1 er= new Proyecto1(nodos,aristas);
        Proyecto1 gil= new Proyecto1(nodos,proba);
        Proyecto1 geo = new Proyecto1(nodos,distancia);
        
        er.metodoER();
        gil.metodoGil();
        geo.metodoGografico();
                
        
    }
    
}
